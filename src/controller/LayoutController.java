package controller;


import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.apache.lucene.queryparser.classic.ParseException;


import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.stage.DirectoryChooser;

public class LayoutController {
	@FXML TextField textFieldNom;
	@FXML ListView<String> listView1;
	@FXML Button chooseButton;
	@FXML Button searchButton;	
	
	
	public void chooseDirButtonAction(ActionEvent event) {
		DirectoryChooser dc = new DirectoryChooser();
		File selectedFile = dc.showDialog(null);
		
		if(selectedFile != null) {
			searchButton.setDisable(true);
			Path path = selectedFile.toPath();			
			IndexFiles.myIndex(path);
			searchButton.setDisable(false);
		}else {
			System.out.println("file is not valide!!");
		}
		
	}
	
	public void searchButtonAction(ActionEvent event) throws IOException, ParseException {
		listView1.getItems().clear();
		String query = textFieldNom.getText();
		
		if((!query.equals(""))){
			List<String> result = SearchFiles.mySearch(query);
			listView1.getItems().addAll(result);
		}
	}
	
	@FXML public void handleMouseClick(MouseEvent arg0) {
	    System.out.println("clicked on " + listView1.getSelectionModel().getSelectedItem());
	}
	
	
	
}
